package org.fsfe.liberator;

import java.util.ArrayList;
import java.util.List;

import org.fsfe.liberator.datastructures.NonfreeApplication;
import org.fsfe.liberator.datastructures.NonfreeApplications;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class MainActivity extends RoboFragmentActivity implements LoaderCallbacks<NonfreeApplications> 
{
	@InjectView(R.id.main_flipper) private ViewFlipper flipper;
	@InjectView(R.id.main_status_text) private TextView statusText;
	@InjectView(R.id.main_app_list) private ListView appList;
	@InjectView(R.id.btn_remove_applications) private Button btnRemoveApps;	
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        
        appList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ApplicationListAdapter.attachListenerToListView(appList);
        
        btnRemoveApps.setOnClickListener(new OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				removeApplications();
			}
		});
        
        getSupportLoaderManager().initLoader(0, null, this).forceLoad();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    private void removeApplications()
    {
    	List<String> appPaths = new ArrayList<String>();
    	SparseBooleanArray checkedApps = appList.getCheckedItemPositions();
    	
    	for (int i = 0; i < checkedApps.size(); i++)
    	{
    		if (checkedApps.get(i))
    		{
    			NonfreeApplication app = (NonfreeApplication) appList.getAdapter().getItem(i);
    			Log.d(this.toString(), "Adding " + app.getApkPath());
    			appPaths.add(app.getApkPath());
    		}
    	}
    	
    	ApplicationRemover remover = new ApplicationRemover();
    	remover.execute(appPaths);
    }
    
	@Override
	public Loader<NonfreeApplications> onCreateLoader(int id, Bundle args) 
	{
        // Scan for applications
        setProgressBarIndeterminateVisibility(true);
		statusText.setText(R.string.main_scanning);
		flipper.setDisplayedChild(0);
		return new ApplicationListLoader(this);
	}

	@Override
	public void onLoadFinished(Loader<NonfreeApplications> loader,
			NonfreeApplications result) 
	{
		ApplicationListAdapter adapter = new ApplicationListAdapter(this, result);
		appList.setAdapter(adapter);
		setProgressBarIndeterminateVisibility(false);
		flipper.setDisplayedChild(1);
	}

	@Override
	public void onLoaderReset(Loader<NonfreeApplications> loader) 
	{
		// TODO
	}
}
