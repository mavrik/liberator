package org.fsfe.liberator;

import java.util.HashMap;

import org.fsfe.liberator.datastructures.NonfreeApplication;
import org.fsfe.liberator.datastructures.NonfreeApplications;
import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

public class ApplicationListLoader extends AsyncTaskLoader<NonfreeApplications> 
{
	private Context context;
	
	public ApplicationListLoader(Context context) 
	{
		super(context);
		this.context = context;
	}

	@Override
	public NonfreeApplications loadInBackground() 
	{
		HashMap<String, String> appsToScan = new HashMap<String, String>();
		
		try
		{
			XmlResourceParser xmlParser = context.getResources().getXml(R.xml.nonfree_applications);
			xmlParser.next();
			int eventType = xmlParser.getEventType();
			
			String name = null;
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				switch(eventType)
				{
					case XmlPullParser.START_TAG:
						assert(xmlParser.getName() == "string");
						name = xmlParser.getAttributeValue(null, "name");
						break;
					case XmlPullParser.TEXT:
						String path = xmlParser.getText();
						Log.d(this.toString(), "App " + name + " at " + path);
						appsToScan.put(name, path);
						break;
					case XmlPullParser.END_TAG:
						assert(xmlParser.getName() == "string");
						break;
					default:
						break;
				}
				
				eventType = xmlParser.next();
			}
		}
		catch (Exception e)
		{
			Log.e(this.toString(), "Failed to parse application list file.", e);
			return null;
		}
		
		// Inspect found package
		
		NonfreeApplications apps = new NonfreeApplications();
		PackageManager manager = context.getPackageManager();
		
		for (String key : appsToScan.keySet())
		{
			PackageInfo pi = manager.getPackageArchiveInfo(appsToScan.get(key), 0);
			if (pi == null)
			{
				Log.w(this.toString(), "Failed to parse package " + appsToScan.get(key));
				continue;
			}
			
			
			try
			{
				Drawable icon = manager.getApplicationIcon(pi.applicationInfo.packageName);
				NonfreeApplication app = new NonfreeApplication(key, 
																pi.applicationInfo.packageName, 
																appsToScan.get(key),
																icon);
				apps.addApplication(app);
			}
			catch (NameNotFoundException e)
			{
				Log.e(this.toString(), "Error while loading icon!", e);
			}

		}
	
		return apps;
	}

}
