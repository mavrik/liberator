package org.fsfe.liberator;

import java.util.List;

import org.fsfe.liberator.datastructures.NonfreeApplication;
import org.fsfe.liberator.datastructures.NonfreeApplications;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ApplicationListAdapter extends BaseAdapter 
{
	public static void attachListenerToListView(final ListView view)
	{
		view.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View item, int position, long id) 
			{
				ViewWrapper vw = (ViewWrapper) item.getTag();
				vw.checked = !vw.checked;
				view.setItemChecked(position, vw.checked);
				vw.checkBox.setChecked(vw.checked);
			}
		});
	}
	
	private static class ViewWrapper 
	{
		public ImageView iw;
		public TextView name;
		public TextView pkg;
		public CheckBox checkBox;
		public boolean checked = false;
	}
	
	private Activity context;
	private List<NonfreeApplication> nonfreeApps;
	
	public ApplicationListAdapter(Activity context, NonfreeApplications apps) 
	{
		super();
		this.context = context;
		this.nonfreeApps = apps.getApplications();
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		NonfreeApplication app = nonfreeApps.get(position);
		View view = null;
		
		if (convertView == null)
		{
			LayoutInflater inflater = context.getLayoutInflater();
			view = inflater.inflate(R.layout.list_application_item, parent, false);
			ViewWrapper vw = new ViewWrapper();
			vw.iw = (ImageView)view.findViewById(R.id.img_app_icon);
			vw.name = (TextView)view.findViewById(R.id.txt_app_name);
			vw.pkg = (TextView)view.findViewById(R.id.txt_pkg_name);
			vw.checkBox = (CheckBox)view.findViewById(R.id.checkbox_selected);
			view.setTag(vw);
		}
		else
		{
			view = convertView;
		}
		
		ViewWrapper vw = (ViewWrapper) view.getTag();
		vw.iw.setImageDrawable(app.getIcon());
		vw.name.setText(app.getName());
		vw.pkg.setText(app.getPackageName());
		vw.checkBox.setChecked(vw.checked);
		return view;
	}

	@Override
	public int getCount() 
	{
		return nonfreeApps.size();
	}

	@Override
	public NonfreeApplication getItem(int position) 
	{
		return nonfreeApps.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}
}
