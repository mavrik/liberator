package org.fsfe.liberator.datastructures;

import java.util.ArrayList;
import java.util.List;

public class NonfreeApplications {
	private List<NonfreeApplication> nonfreeApps;
	
	public NonfreeApplications()
	{
		nonfreeApps = new ArrayList<NonfreeApplication>();
	}
	
	public void addApplication(NonfreeApplication app)
	{
		nonfreeApps.add(app);
	}
	
	public List<NonfreeApplication> getApplications()
	{
		return nonfreeApps;
	}
}
