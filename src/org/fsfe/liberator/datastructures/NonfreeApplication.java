package org.fsfe.liberator.datastructures;

import android.graphics.drawable.Drawable;

public class NonfreeApplication 
{
	private String name;
	private String packageName;
	private String apkPath;
	private Drawable icon;
	
	public NonfreeApplication(String name, String packageName, String apkPath, Drawable icon) 
	{
		super();
		this.name = name;
		this.packageName = packageName;
		this.apkPath = apkPath;
		this.icon = icon;
	}
	
	public String getPackageName() {
		return packageName;
	}

	public String getName() 
	{
		return name;
	}
	
	public String getApkPath() 
	{
		return apkPath;
	}

	public Drawable getIcon() {
		return icon;
	}

	@Override
	public String toString() {
		return "NonfreeApplication [packageName=" + packageName + ", apkPath="
				+ apkPath + "]";
	}
}
