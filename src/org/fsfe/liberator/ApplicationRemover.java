package org.fsfe.liberator;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class ApplicationRemover extends AsyncTask<List<String>, Integer, Integer> 
{
	public static final int STATUS_OK = 0;
	public static final int STATUS_NO_ROOT = 1;
	public static final int STATUS_IO_ERROR = 2;
	
	private static boolean hasRoot()
	{
		Log.i("ApplicationRemover", "Attempting to gain root access...");
		
		Process suProc;
		DataOutputStream os = null;
		try
		{
			suProc = Runtime.getRuntime().exec("su");
			DataInputStream is = new DataInputStream(suProc.getInputStream());
			os = new DataOutputStream(suProc.getOutputStream());
			
			if (is != null && os != null)
			{
				// Get current user ID to check for root privileges
				os.writeBytes("id\n");
				os.flush();
				
				String currentId = is.readLine();
				if (currentId == null)
				{
					Log.e("ApplicationRemover", "Could not gain root access, probably user abort!");
					return false;
				}
				else if (currentId.contains("uid=0"))
				{
					Log.i("ApplicationRemover", "Root access gained, continuing...");
					return true;
				}
				else
				{
					Log.e("ApplicationRemover", "Unable to gain root access!");
					return false;
				}
			}
		}
		catch (Exception e)
		{
			Log.e("ApplicationRemover", "Could not gain root access, exception!", e);
			return false;
		}
		finally
		{
			// Close opened shell 
			if (os != null)
			{
				try
				{
					os.writeBytes("exit\n");
					os.flush();
					os.close();
				}
				catch (Exception e) {};
			}
		}
	
		return false;
	}
	
	Handler progressCallback = null;
	
	public ApplicationRemover()
	{
		super();
	}
	
	public ApplicationRemover(Handler progressCallback)
	{
		this.progressCallback = progressCallback;
	}
	
	private String escapeForRm(String filepath)
	{
		String escapedQuotes = filepath.replace("'", "'\\''");
		return "'" + escapedQuotes + "'";
	}
	
	private int runCommand(String command, DataInputStream dis, DataOutputStream dos) throws IOException
	{
		dos.writeBytes(command + "\n");
		dos.flush();
		dis.skipBytes(dis.available());
		dos.writeBytes("echo $?\n");
		dos.flush();
		String sReturnCode = dis.readLine();
		int returnCode = Integer.getInteger(sReturnCode.trim());
		return returnCode;
	}
	
	@Override
	protected Integer doInBackground(List<String>... appsArg) 
	{
		List<String> appPaths = appsArg[0];
		
		if (!hasRoot())
		{
			return STATUS_NO_ROOT;
		}
		
		int progress = 0;
		
		// Get root access
		Process su = null;
		DataInputStream dis = null;
		DataOutputStream dos = null;
		
		try
		{
			su = Runtime.getRuntime().exec("su");
			dis = new DataInputStream(su.getInputStream());
			dos = new DataOutputStream(su.getOutputStream());
		
			// Remount system FS read-write
			int returnCode = runCommand("mount -o remount,rw /system", dis, dos);
			if (returnCode != 0)
			{
				Log.e("ApplicationRemover", "System remount command failed " + returnCode);
				return STATUS_IO_ERROR;
			}
			
			for (String appPath : appPaths)
			{
				Log.d("ApplicationRemover", "Removing " + appPath);
				
				returnCode = runCommand("rm " + escapeForRm(appPath), dis, dos);
				if (returnCode != 0)
				{
					Log.e("ApplicationRemover", "Failed to delete " + appPath + " code " + returnCode);
					return STATUS_IO_ERROR;
				}
				
				progress++;
				publishProgress(progress);
			}
		}
		catch (IOException e)
		{
			Log.e("ApplicationRemover", "File removal failure!", e);
			return STATUS_IO_ERROR;
		}
		finally
		{
			if (dos != null)
			{
				try 
				{
					dos.writeBytes("exit\n");
					dos.flush();
				} catch (IOException e) {}
			}
		}
		
		return STATUS_OK;
	}

	@Override
	protected void onProgressUpdate(Integer... values) 
	{
		super.onProgressUpdate(values);
		
		if (this.progressCallback != null)
		{
			int progress = values[0];
			this.progressCallback.sendEmptyMessage(progress);
		}
	}

}
